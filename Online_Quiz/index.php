<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Online Quiz</title>
    <link rel="shortcut icon" href="assets/img/brain.png">
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap" rel="stylesheet">
   
    
</head>
<body>
    <!--Container-->
    <br>
        <div class = "card" style = "width:50%;margin:0 auto;height:80vh;">
            <div class = "card-header bg-dark text-light">
                <h1 align=center> <img src = "assets/img/brain.png" style = "width:150px;" > </h1>
                <h1 align=center " style = "font-family: 'Abril Fatface', cursive;font-size:50px;"> Computer Aided Examination</h1>
            </div>
            <div class="card-body bg-light"> 
                <!--Form-->
                
                <br>
                <br>
                <br>
                <div class = "card" style = "text-align:center;font-family:goergia;font-size:30px;border-radius:20px;width:50%;margin:0 auto; ">
                    <div class = "card-header bg-primary text-dark" style = "font-weight:bold;border-top-left-radius:20px;border-top-right-radius:20px;">
                        ADMIN
                    </div>
                    <div class = "card-body">
                    <a href = "admin_login.php"> Log in </a>
                    </div>
                </div>
                <br>
                <div class = "card" style =  "text-align:center;font-family:Georgia;font-size:30px;border-radius:20px;width:50%;margin:0 auto; ">
                    <div class = "card-header bg-primary text-dark" style = "font-weight:bold;border-top-left-radius:20px;border-top-right-radius:20px;">
                        USER
                    </div>
                    <div class = "card-body">
                    <a href = "user_login.php"> Log in </a>
                    </div>
                </div>
                
                

            </div>
        </div>
    
 
</body>
</html>