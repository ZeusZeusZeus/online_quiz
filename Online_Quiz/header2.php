<?php
    session_start();
    include "perfect_function.php";
    if(isset($_SESSION['username'])){
        $username = $_SESSION['username'];
    }
    if(isset($_SESSION['user_id'])){
        $user_id = $_SESSION['user_id'];
    }
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <title>Online Quiz</title>
</head>
<body style = "background-color:white">

    <nav class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar" style = "width:100%;height:100px;color:white; background-color:#2D283E;margin-top:-10px;border-radius:3px;font-size:30px;font-family: Verdana;">

  
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="userhome.php"> <i class="fas fa-home"></i> Home</a>
                </li>
               
                <li class="nav-item">
                    <a class="nav-link" href="quizrecord.php"><i class="fa fa-history" aria-hidden="true"></i> Quiz History</a>
                </li>
               
            </ul>
            <ul class="nav navbar-nav ml-auto" style = "margin-right:20px;">
            <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fas fa-user"></i> <?=$username?></a>
                    <div class="dropdown-menu">
                        <form action = "account_info.php?id=<?=$user_id?>" method = "POST">
                        <button class="dropdown-item">Account Setting</button>
                        </form>
                        <button class="dropdown-item" name = "logout" onClick='myFunction()' >Log Out</button>
                        <script>
                        function myFunction() {
                       
                        var r = confirm("Log out account?");
                        if (r == true) {
                            window.location.href = "logout.php";
                        }
                        }
                       
                        </script>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
    <br>