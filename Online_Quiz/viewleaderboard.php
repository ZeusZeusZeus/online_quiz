<?php
 include "header.php";
 if (!isset($_SESSION['admin_id'])){
    header("Location:index.php");
}
 $qtype_id = $_GET['qtype_id'];
 $topic_id = $_GET['topic_id'];
 date_default_timezone_set('Asia/Manila');
 $date = date('y/m/d h:i:s a', time());
 $topic = "SELECT Topic_Name from topics where Topic_ID = '$topic_id'";
 $topic_query = custom_query($topic);
 foreach($topic_query as $key => $row){
     $topicname = $row['Topic_Name'];
 }
 $type = "SELECT Question_Type from questions_type where Question_Type_ID = '$qtype_id'";
 $type_query = custom_query($type);
 foreach($type_query as $key => $row){
     $type = $row['Question_Type'];
 }
 ?>
  <div class = "container" style = "border:none;">
  <div class = "card-header bg-primary text-light" style = "border-radius:20px;">
    <h1 align=center> <?=$topicname." (".$type.")"?> Leaderboard </h1>
  </div>
    <br>
    
      
        
            <table class="table table-bordered" align=center style = "margin-top:10px;text-align:center;font-family:verdana;">
            
                <thead class = "thead bg-dark text-light" style = "font-size:20px;">
                    <tr>
                        <th style = "width:20%;">
                            Rank
                        </th>
                        <th style = "width:50%;">
                            Name
                        </th>
                        <th style = "width:30%;">
                            Score
                        </th>
                    </tr>
                </thead>
                <?php
                    $rank  = 0;
                    $quiz = "Select results.score, quiz.quiz_id, users.Firstname, users.Lastname from quiz join results on quiz.quiz_id = results.quiz_id join users on quiz.user_id = users.user_id where topic_id = '$topic_id' && Question_Type_ID = '$qtype_id' ORDER BY score DESC, time DESC limit 10";
                    $quizquery = custom_query($quiz);
                    foreach($quizquery as $key => $row){
                        $quiz_id = $row['quiz_id'];
                        $name = $row['Firstname']." ".$row['Lastname'];
                        $rank +=1;
                        $score = "Select score from results where quiz_id = '$quiz_id' ORDER BY results.score desc";
                        
                        $scorequery = custom_query($score);
                        foreach($scorequery as $key =>$row){
                            $score = $row['score'];
                        ?>
                        <tr>
                            <td>
                            <?=$rank?>
                            </td>
                        <td>
                            <?=$name?>
                        </td>
                        <td>
                            <?=$score?>
                        </td>
                        </tr>        
                <?php
                        }
                    }
                ?>
            </table>

            <h1 align=center> <a href = "leaderboard.php" class = "btn btn-warning" style = "width:150px;"><i class = "fas fa-arrow-left"> Back </i> </a> </h1>
        
   
    
  </div>