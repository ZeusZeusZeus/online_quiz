<?php
    session_start();
    function alert($msg) {
        echo "<script type='text/javascript'>alert('$msg');</script>";
    }

    if(isset($_SESSION['emailtaken'])){
        alert("Email alreay taken");
        unset($_SESSION['emailtaken']);
    }
    if(isset($_SESSION['usernametaken'])){
        alert("Username alreay taken");
        unset($_SESSION['usernametaken']);
    }
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>

.centered-form{
	margin-top: 60px;
}

.centered-form .panel{
	background: rgba(255, 255, 255, 0.8);
	box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
}
</style>

<div class="container" style = "margin:0 auto;">
        <div class="row centered-form" style = "width:80%;margin:0 auto;">
        <div class="col-sm-12" style = "margin:20 auto;">
        <br>
        	<div class="panel panel-default" style = "font-family:verdana;font-size:20px;">
        		<div class="panel-heading" style = "font-family:Georgia;">
			    		<h1 align=center>Create Account </h1>
			 	</div>
			 			<div class="panel-body">
			    		<form role="form" action = "registerproc.php" method = "POST">
			    			<div class="row">
			    				<div class="col-md-6">
			    					<div class="form-group">
			                            <input type="text" name="firstname" id="first_name" class="form-control input-lg" placeholder="First Name" required>
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6">
			    					<div class="form-group">
			    						<input type="text" name="lastname" id="last_name" class="form-control input-lg" placeholder="Last Name" required>
			    					</div>
			    				</div>
			    			</div>

			    			<div class="form-group">
			    				<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" required>
			    			</div>
                            <div class="form-group">
			    				<input type="username" name="username" id="username" class="form-control input-lg" placeholder="Username" required>
			    			</div>
                            <div class="form-group" >
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" required>
                            </div>
                            
			    		
			    			
			    			<button type="submit" class="btn btn-info btn-block input-lg" style = "font-size:25px;color:black;font-family:georgia;"> Register </button>
                            <a href = "user_login.php" class="btn btn-warning btn-block form-control input-lg" style = "font-size:25px;color:black;font-family:georgia;"> Cancel </a>
			    		
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>