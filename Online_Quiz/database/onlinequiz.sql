-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 04:19 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlinequiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `password` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `Firstname` varchar(65) NOT NULL,
  `Lastname` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `email`, `Firstname`, `Lastname`) VALUES
(1, 'admin', 'PASSWORD', 'admin@gmail.com', 'ADMIN', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `Answer_ID` int(11) NOT NULL,
  `Answer` varchar(65) NOT NULL,
  `Question_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`Answer_ID`, `Answer`, `Question_ID`) VALUES
(15, 'PHP: Hypertext Preprocessor', 18),
(16, 'Angular', 19),
(17, 'Rasmus Lerdorf', 20),
(18, 'True', 21),
(20, 'True', 23),
(22, 'Hypertext Markup Language', 25),
(23, 'H1', 26),
(24, 'Br', 27),
(25, 'False', 28),
(26, 'False', 29),
(27, 'True', 30),
(28, '6', 31),
(29, 'True', 32),
(30, 'True', 33),
(31, 'print', 34),
(32, 'True', 35);

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `Choice_ID` int(11) NOT NULL,
  `Choice` varchar(100) NOT NULL,
  `Question_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`Choice_ID`, `Choice`, `Question_ID`) VALUES
(43, 'Philippines', 18),
(44, 'PHP: Hypertext Preprocessor', 18),
(45, 'Philippine Home Page', 18),
(46, 'Philippine Heroes Page', 18),
(47, 'Laravel', 19),
(48, 'CodeIgniter', 19),
(49, 'Yii', 19),
(50, 'Angular', 19),
(51, 'Rasmus Lerdorf', 20),
(52, 'Larry Wall', 20),
(53, 'Rob Pike', 20),
(54, 'Grace Hopper', 20),
(55, 'True', 21),
(56, 'False', 21),
(59, 'True', 23),
(60, 'False', 23),
(63, 'Hypertext Markup Language', 25),
(64, ' \r\nHyperlinks and Text Markup Language', 25),
(65, ' \r\nHome Tool Markup Language', 25),
(66, 'How To Make Lumpia', 25),
(67, 'Heading', 26),
(68, 'Span', 26),
(69, 'H1', 26),
(70, 'Head', 26),
(71, 'Break', 27),
(72, 'NextLine', 27),
(73, 'LB', 27),
(74, 'Br', 27),
(75, 'True', 28),
(76, 'False', 28),
(77, 'True', 29),
(78, 'False', 29),
(79, 'True', 30),
(80, 'False', 30),
(81, '8', 31),
(82, '5', 31),
(83, '6', 31),
(84, '4', 31),
(85, 'True', 32),
(86, 'False', 32),
(87, 'True', 33),
(88, 'False', 33),
(89, 'echo', 34),
(90, 'print', 34),
(91, 'console.write', 34),
(92, 'println', 34),
(93, 'True', 35),
(94, 'False', 35);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `Question_ID` int(11) NOT NULL,
  `Question` varchar(100) NOT NULL,
  `Topic_ID` int(11) NOT NULL,
  `Question_Type_ID` int(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`Question_ID`, `Question`, `Topic_ID`, `Question_Type_ID`) VALUES
(18, 'PHP stands for', 6, 1),
(19, 'Which of the following is not a PHP framework', 6, 1),
(20, 'Who is the founder of PHP', 6, 1),
(21, 'PHP is a scripting language', 6, 2),
(23, '\"Personal Home Page\" is the original name of PHP', 6, 2),
(25, 'HTML stands for', 7, 1),
(26, 'Choose the correct HTML tag for the largest heading', 7, 1),
(27, ' What is the correct HTML tag for inserting a line break', 7, 1),
(28, 'HTML is a programming language', 7, 2),
(29, 'HTML is developed by Mark Zuckerberg', 7, 2),
(30, 'The tags of HTML are surrounded by angular bracket', 7, 2),
(31, ' How many types of heading does an HTML contain', 7, 1),
(32, 'Python is case-sensitive', 8, 2),
(33, 'List allows duplicate number', 8, 2),
(34, 'What is the command use to print in python', 8, 1),
(35, 'Function starts with the word \"function\"?', 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions_type`
--

CREATE TABLE `questions_type` (
  `Question_Type_ID` int(11) NOT NULL,
  `Question_Type` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_type`
--

INSERT INTO `questions_type` (`Question_Type_ID`, `Question_Type`) VALUES
(1, 'Multiple Choice'),
(2, 'True or False');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `quiz_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `Question_Type_ID` int(11) NOT NULL,
  `Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quiz_id`, `user_id`, `topic_id`, `Question_Type_ID`, `Time`) VALUES
(155, 1, 7, 1, '2020-03-01 22:06:47'),
(156, 1, 7, 1, '2020-03-02 12:36:21'),
(160, 1, 6, 1, '2020-03-09 20:49:03'),
(161, 1, 7, 2, '2020-03-09 20:50:18'),
(162, 1, 7, 1, '2020-03-09 20:52:43'),
(163, 2, 6, 2, '2020-03-09 21:25:29'),
(164, 2, 8, 2, '2020-03-10 20:17:10'),
(165, 1, 8, 2, '2020-03-12 19:19:07'),
(166, 1, 8, 2, '2020-03-13 19:20:20'),
(167, 1, 7, 1, '2020-03-14 20:12:09'),
(168, 1, 6, 1, '2020-03-14 20:12:41'),
(169, 1, 7, 2, '2020-03-14 20:12:58'),
(170, 1, 6, 1, '2020-03-15 18:22:56'),
(171, 7, 7, 1, '2020-03-17 21:28:35'),
(172, 7, 6, 2, '2020-03-17 21:29:27'),
(173, 7, 8, 2, '2020-03-17 21:29:53');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `result_id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`result_id`, `quiz_id`, `score`) VALUES
(89, 155, 3),
(90, 156, 1),
(91, 160, 3),
(92, 161, 3),
(93, 162, 1),
(94, 163, 1),
(95, 164, 2),
(96, 165, 1),
(97, 166, 0),
(98, 167, 0),
(99, 168, 2),
(100, 169, 0),
(101, 170, 3),
(102, 171, 4),
(103, 172, 3),
(104, 173, 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `Topic_ID` int(11) NOT NULL,
  `Topic_Name` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`Topic_ID`, `Topic_Name`) VALUES
(7, 'HTML'),
(6, 'PHP'),
(8, 'PYTHON'),
(12, 'SQL');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `password` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `Firstname` varchar(65) NOT NULL,
  `Lastname` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `Firstname`, `Lastname`) VALUES
(1, 'zeus', '123', 'zeus.marou@gmail.com', 'Marou', 'Marigocio'),
(2, 'ZeusZeusZeus', '123', 'zmmarigocio@gmail.com', 'Zeus', 'Marigocio'),
(7, 'BG', '123', 'biggie@gmail.com', 'Armand ', 'Liong');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`Answer_ID`),
  ADD KEY `Question_ID_2` (`Question_ID`);

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`Choice_ID`),
  ADD KEY `Question_ID` (`Question_ID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`Question_ID`),
  ADD KEY `Topic_ID` (`Topic_ID`),
  ADD KEY `Topic_ID_2` (`Topic_ID`),
  ADD KEY `Question_Type_ID` (`Question_Type_ID`);

--
-- Indexes for table `questions_type`
--
ALTER TABLE `questions_type`
  ADD PRIMARY KEY (`Question_Type_ID`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`quiz_id`),
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `Question_Type_ID` (`Question_Type_ID`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`result_id`),
  ADD UNIQUE KEY `quiz_id` (`quiz_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`Topic_ID`),
  ADD KEY `Topic_Name` (`Topic_Name`),
  ADD KEY `Topic_Name_2` (`Topic_Name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `Answer_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `Choice_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `Question_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `questions_type`
--
ALTER TABLE `questions_type`
  MODIFY `Question_Type_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `quiz_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `result_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `Topic_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`Question_ID`) REFERENCES `questions` (`Question_ID`);

--
-- Constraints for table `choices`
--
ALTER TABLE `choices`
  ADD CONSTRAINT `choices_ibfk_1` FOREIGN KEY (`Question_ID`) REFERENCES `questions` (`Question_ID`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`Topic_ID`) REFERENCES `topics` (`Topic_ID`),
  ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`Question_Type_ID`) REFERENCES `questions_type` (`Question_Type_ID`);

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `quiz_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `quiz_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`Topic_ID`);

--
-- Constraints for table `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `results_ibfk_1` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`quiz_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
