<?php
     session_start();
     function alert($msg) {
         echo "<script type='text/javascript'>alert('$msg');</script>";
     }
 
     if(isset($_SESSION['error'])){
         alert("Username or Password is Incorrect");
         unset($_SESSION['error']);
     }
     if (isset($_SESSION['user_id'])){
        header("Location:userhome.php");
    }
    if(isset($_SESSION['created'])){
        alert("Account has been succesfully created");
        unset($_SESSION['error']);
    }
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
    <title>Online Quiz</title>
</head>
</head>
<body>
<br>
    <div class = "container" style = "width:30%;margin:0 auto;border-style:outset;">
    <br>
            <!-- Default form login -->
        <form class="text-center" action="user_login_proc.php" method = "POST">
        <h1> <img src = "assets/img/user_logo.png" style = "width:150px;"> </h1>
        <h2> USER </h1>
      

        <!-- Email -->
        <input type="text" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Username" name = "username" required>

        <!-- Password -->
        <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name = "password" required>

        

        <!-- Sign in button -->
        <button class="btn btn-info btn-block my-4" type="submit" style = "font-size:25px;">Log in</button>

        <!-- Register -->
        <p>Not a member?
            <a href="registration.php">Register</a>
        </p>

     
        <!-- Default form login -->
    </div>
    </form>
    <br>
    <h2 align=center> <a href = "index.php" class = "btn btn-warning" style = "width:150px;"> Back </a> </h1>
</body>
</html>