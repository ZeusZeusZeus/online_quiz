<?php

    include "header2.php";
    if (!isset($_SESSION['user_id'])){
        header("Location:index.php");
    }
    $topic_id = $_GET['topic_id'];
    date_default_timezone_set('Asia/Manila');
    $date = date('y/m/d H:i:s a', time());
    $_SESSION['date'] = $date;
    

    
    

?>

    <h1 align=center style="font-family:Georgia"> Welcome to Online Quiz </h1>

    <div class = "card" style = "width:60%;margin:0 auto;">
 
        <div class = "card-header bg-primary text-light">
            <h1 align=center  style = "font-family:Verdana;"> Select Question Type </h1>
        </div>
        <div class = "card-body">
            <table class = "table table-striped " style = "font-family:verdana;">
                <thead class = "thead" style = "font-weight:bold;font-size:30px;">
                    <tr>
                        <th>
                            Question Type
                        </th>
                        <th>
                            Total Questions
                        </th>
                        <th>
                            Action

                        </th>
                    </tr>
                </thead>
                <?php
                $qtype = "Select  * from questions_type";
                $qtypequery = custom_query($qtype);
                foreach($qtypequery as $key =>$row){
                    $qtype_ID= $row['Question_Type_ID'];
                    $questiontype = $row['Question_Type'];

                    $items = "SELECT COUNT(Question) AS Total FROM questions  join questions_type on questions.Question_Type_ID = questions_type.Question_Type_ID  WHERE Question_Type = '$questiontype' && Topic_ID = '$topic_id'";
                    $itemquery = custom_query($items);
                    foreach($itemquery as $key =>$row){
                        $total = $row['Total'];

                    ?>
                    <tr>
                        <th>
                            <?=$questiontype?>
                        </th>
                        <td>
                            <?=$total?>
                        </td>
                        <td>
                    <?php 
                        if ($total  != 0) { ?> 
                            <a href = "selectproc.php?topic_id=<?=$topic_id?>&qtype_id=<?=$qtype_ID?>" class = "btn btn-primary" style = "width:150px;"><i class="fas fa-play"></i>  Start </a> <?php 
                        }else {  ?>
                            <a href = "#" class = "btn btn-danger"><i class="fas fa-exclamation-triangle"></i>   Not Available </a>

                     <?php     } ?>
                        </td>
                    </tr>    
            <?php    }
                }
            ?>

            </table>
        </div>
        
    </div>
    <br>
    <p align=center> <a href = "userhome.php" class = "btn btn-warning" style = "font-size:18px;width:150px;" ><i class = "fa fa-arrow-left"> Back </i></a> </p>
