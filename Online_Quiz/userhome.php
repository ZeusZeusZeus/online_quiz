
<!DOCTYPE html>
<html lang="en">
<?php

    include "header2.php";
    if (!isset($_SESSION['user_id'])){
        header("Location:index.php");
    }
    unset($_SESSION['submitted']);
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/brain.png">
    <title>Online Quiz</title>
</head>
<body>
    <h1 align=center style="font-family:Georgia"> Welcome to Online Quiz </h1>

        <div class = "card" style = "width:60%;margin:0 auto;">
                <div class = "card-header bg-primary text-light" style = "font-family:verdana;"> <h1 align=center> Topics Available  </h1> </div>
                <div class = "card-body">  
                <table class = "table table-striped" style = "font-family:verdana;text-align:center;">
                    <thead  class = "thead" style = "font-weight:bold;font-size:30px;" >
                        <tr >
                            <th>
                                Topic
                            </th>
                          
                            <th>
                                Action

                            </th>
                        </tr>
                    </thead>
                    <?php
                    $topic = "Select  * from topics";
                    $topicquery = custom_query($topic);
                    foreach($topicquery as $key =>$row){
                        $topic_id = $row['Topic_ID'];
                        $topic_name = $row['Topic_Name'];

                        $question = "SELECT COUNT(Question_ID) AS Total FROM questions where Topic_ID = '$topic_id'";
                        $questionquery = custom_query($question);
                        foreach($questionquery as $key =>$row){
                            $total = $row['Total'];

                        ?>
                        <tr>
                            <?php if ($total!=0){ ?>
                            <td style = "font-weight:bold;">
                                <?= $topic_name ?>
                            </td>
                           
                            <td>
                                <a href = "questiontype.php?topic_id=<?=$topic_id?>" class = "btn btn-primary" style = "width:170px"><i class="fas fa-hand-pointer"></i>  Select </a>
                            </td>
                            <?php } ?>
                        </tr>    
                <?php    }
                    }
                ?>

                </table>
            </div>
    </div>
</body>
</html>
    
    