<?php
    include "header.php";
    $topic_id = $_GET['topic_id'];
    if (!isset($_SESSION['admin_id'])){
        header("Location:index.php");
    }

    if (!isset($_GET['topic_id'])){
        header("Location:topic.php");
    }

    $topicname = "Select * from topics where Topic_ID = '$topic_id'";
    $topicquery = custom_query($topicname);
    foreach($topicquery as $key => $row){
        $Topic_Name = $row['Topic_Name'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/brain.png">
    <title>Online Quiz</title>
</head>

<body>
    <div class = "card" style = "width:60%; margin:0 auto;">
            <div class = "card-header bg-primary text-light">
            <h1 align=center style = "font-family:Verdana;font-size:50px;"> <?=$Topic_Name?> </h1> 
                
            </div>
            <br>
            
            <div class = "card-body">    
                <table  class="table" cellspacing ="0" role = "grid" style = "width:100%;margin:0 auto;font-family:verdana;">
                    <thead class = "thead" style = "font-size:30px;">
                        <tr align=center>
                        <th scope="col">Quiz Type</th>
                        
                        
                        <th scope="col">Quiz Items</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
     
                    $topics = "Select * from questions_type ";
                    $topicsquery = custom_query($topics);
                    foreach($topicsquery as $key =>$row){

                        $questiontype_id = $row['Question_Type_ID'];
                    
                    
                        $questiontype = $row['Question_Type'];
                        $items = "SELECT COUNT(Question) AS NumberOfItems FROM questions  join questions_type on questions.Question_Type_ID = questions_type.Question_Type_ID  WHERE Question_Type = '$questiontype' && Topic_ID = '$topic_id'";
                        $itemsquery = custom_query($items);
                        foreach ($itemsquery as $key =>$row){
                        
                            $numberofitems = $row['NumberOfItems'];
                        ?>
                            <tr align=center>
                            <th scope="row"><?=$questiontype?></th>
                            <td><?=$numberofitems?>  </td> 
                            <td><a href = "viewquestion.php?question_type_id=<?=$questiontype_id?>&topic_id=<?=$topic_id?>" class = "btn btn-primary" style = "width:100px;font-family:verdana"><i class="far fa-eye"></i> View </a></td>
                            
                    
                        </tr>
                    <?php
                        }
                        ?>
            
                    

                        
                    <?php 
                    }   
                    ?>
                    
                        
                    </tbody>
                </table>
                
            </div>  
              
    </div>
    <br>
    <p align=center style = "margin-right:10px;">  <a href="javascript:history.go(-1)" class = "btn btn-warning" style = "width:150px;"><i class = "fas fa-arrow-left"> Back </i> </a> </p>
    
</body>
</html>
        
    
